#!/usr/bin/perl

use strict;
use warnings;

$|=1;

if( $#ARGV < 3 )
{
	die( "Usage: $0 blast_result file_genome window_size file_out\n" );
}

my( $FN_blast ) = $ARGV[0];
my( $FN_genome ) = $ARGV[1];
my( $window_size ) = $ARGV[2];
my( $FN_out ) = $ARGV[3];

my( $id );
my( $sub_pol );
my( $sub_seq );
my( $sub_start, $sub_end );
my( $length );
my( $left, $left_size, $left_seq );
my( $right, $right_size, $right_seq );
my( $sub_seq_redo );
my( $whole_seq );
my( $FH );
my( @POL ) = ("Minus","Plus");
my( %genome );
my( %blastResult );

SEQ_FASTA_ReadFile( $FN_genome, \%genome, \&getID );
SEQ_BLAST_ParseBLASTResult_Total( "blastn", $FN_blast, \%blastResult );

open( $FH, ">$FN_out" ) or die( "Cannot write to file: $FN_out\n" );
for( my $i=0; $i<=$#{$blastResult{result}}; $i++ )
{
	$id = $blastResult{result}[$i]{id};

	if( exists($genome{$id}) )
	{
		if( !CheckSeq($genome{$id},$blastResult{result}[$i]) )
		{
			printf( STDERR "Error: result %d, subject sequence does not match genome sequence: %s\t SKIPPED\n", $i, $id );
			last;
		}

		$sub_pol = $blastResult{result}[$i]{alignment}{subject_pol};
		$sub_seq = $blastResult{result}[$i]{alignment}{subject};
		$sub_start = $blastResult{result}[$i]{alignment}{subject_start};
		$sub_end = $blastResult{result}[$i]{alignment}{subject_end};
		$length = length($genome{$id}[0]);

		if( $sub_pol == 1 )
		{
			if( $sub_start > $window_size )
			{
				$left = $sub_start - $window_size;
				$left_size = $window_size;
			}
			else
			{
				$left=1;
				$left_size = $sub_start-1;
			}
	
			$right = $sub_end+1;
			$right_size = $sub_end+$window_size<=$length ? $window_size : $length-$sub_end;

			$left_seq = substr($genome{$id}[0],$left-1,$left_size);
			$right_seq = substr($genome{$id}[0],$right-1,$right_size);
			$whole_seq = substr($genome{$id}[0],$left-1,$right+$right_size-$left);
			$sub_seq_redo = substr($genome{$id}[0],$sub_start-1,$sub_end-$sub_start+1);
		}
		else
		{
			$left = $sub_start+1;
			$left_size = $sub_start+$window_size<=$length ? $window_size : $length-$sub_start;

			if( $sub_end > $window_size )
                        {
                                $right = $sub_end - $window_size;
                                $right_size = $window_size;
                        }
                        else
                        {
                                $right=1;
                                $right_size = $sub_end-1;
                        }

			$left_seq = substr($genome{$id}[0],$left-1,$left_size);
			$right_seq = substr($genome{$id}[0],$right-1,$right_size);
			$sub_seq_redo = substr($genome{$id}[0],$sub_end-1,$sub_start-$sub_end+1);
			$whole_seq = substr($genome{$id}[0],$right-1,$left+$left_size-$right);
		}

		printf( $FH "result %d subject id: %s\n", $i, $id );
		printf( $FH "result %d genome length: %s\n", $i, $length );
		printf( $FH "result %d query aligned seq: %s\n", $i, $blastResult{result}[$i]{alignment}{query} );
		printf( $FH "result %d subject aligned seq: %s\n", $i, $blastResult{result}[$i]{alignment}{subject} );
		printf( $FH "result %d query direction: %s\n", $i, $POL[$blastResult{result}[$i]{alignment}{query_pol}] );
		printf( $FH "result %d query range: %s\t%s\n", $i, $blastResult{result}[$i]{alignment}{query_start}, $blastResult{result}[$i]{alignment}{query_end} );
		printf( $FH "result %d subject direction: %s\n", $i, $POL[$sub_pol] );
		printf( $FH "result %d subject range: %s\t%s\n", $i, $sub_start, $sub_end );

		if( $sub_pol == 1 )
		{
			printf( $FH "result %d subject start G-upstream range: %d\t%d\n", $i, $left, $left+$left_size-1 );
			printf( $FH "result %d subject start G-upstream seq: %s\n", $i, $left_seq );
			printf( $FH "result %d subject range: %d\t%d\n", $i, $sub_start, $sub_end );
			printf( $FH "result %d subject seq: %s\n", $i, $sub_seq_redo );
			printf( $FH "result %d subject end G-downstream range: %d\t%d\n", $i, $right, $right+$right_size-1 );
			printf( $FH "result %d subject end G-downstream seq: %s\n", $i, $right_seq );
			printf( $FH "result %d subject flanking range: %d\t%d\t%d\t%d\n", $i, $left, $left+$left_size-1, $right, $right+$right_size-1 );
			printf( $FH "result %d subject flanking seq: %s\n", $i, $left_seq.$right_seq );
			printf( $FH "result %d subject end G-all seq range: %d\t%d\n", $i, $left, $right+$right_size-1 );
			printf( $FH "result %d subject end G-all seq: %s\n", $i, $whole_seq );
		}
		else
		{
			printf( $FH "result %d subject start G-upstream range: %d\t%d\n", $i, $right, $right+$right_size-1 );
			printf( $FH "result %d subject start G-upstream seq: %s\n", $i, $right_seq );
			printf( $FH "result %d subject range: %d\t%d\n", $i, $sub_start, $sub_end );
			printf( $FH "result %d subject seq: %s\n", $i, $sub_seq_redo );
			printf( $FH "result %d subject end G-downstream range: %d\t%d\n", $i, $left, $left+$left_size-1 );
			printf( $FH "result %d subject end G-downstream seq: %s\n", $i, $left_seq );
			printf( $FH "result %d subject flanking range: %d\t%d\t%d\t%d\n", $i, $right, $right+$right_size-1, $left, $left+$left_size-1 );
			printf( $FH "result %d subject flanking seq: %s\n", $i, $right_seq.$left_seq );
			printf( $FH "result %d subject end G-all seq range: %d\t%d\n", $i, $right, $left+$left_size-1 );
			printf( $FH "result %d subject end G-all seq: %s\n", $i, $whole_seq );
		}
	}
	else
	{
		printf( STDERR "Error: result %d, genome sequence not found for id %s\n", $i, $id );
	}
}
close($FH);

#for( my $i=0; $i<=$#{$blastResult{result}}; $i++ )
#{
#	printf( "result %d id\t%s\n", $i, $blastResult{result}[$i]{id} );
#	printf( "result %d seq_length\t%s\n", $i, $blastResult{result}[$i]{seq_length} );
#	printf( "result %d scanfold\t%s\n", $i, $blastResult{result}[$i]{scanfold} );
#	printf( "result %d align_length\t%s\n", $i, $blastResult{result}[$i]{align_length} );
#	printf( "result %d identity\t%s\n", $i, $blastResult{result}[$i]{identity} );
#	printf( "result %d gap\t%s\n", $i, $blastResult{result}[$i]{gap} );
#	printf( "result %d score\t%s\n", $i, $blastResult{result}[$i]{score} );
#	printf( "result %d expect\t%s\n", $i, $blastResult{result}[$i]{expect} );
#	printf( "result %d alignment query pol\t%d\n", $i, $blastResult{result}[$i]{alignment}{query_pol} );
#	printf( "result %d alignment subject pol\t%d\n", $i, $blastResult{result}[$i]{alignment}{subject_pol} );
#	printf( "result %d alignment query range\t%d\t%d\n", $i, $blastResult{result}[$i]{alignment}{query_start}, $blastResult{result}[$i]{alignment}{query_end} );
#	printf( "result %d alignment subject range\t%d\t%d\n", $i, $blastResult{result}[$i]{alignment}{subject_start}, $blastResult{result}[$i]{alignment}{subject_end} );
#	printf( "result %d alignment query\t%s\n", $i, $blastResult{result}[$i]{alignment}{query} );
#	printf( "result %d alignment subject\t%s\n", $i, $blastResult{result}[$i]{alignment}{subject} );
#}

################################################################################################

sub getID
{
	my( $info ) = @_;
	my( @lineMem );

	$info =~ s/>//;
	@lineMem = split(" ",$info,2);

	return $lineMem[0];
}

sub CheckSeq
{
	my( $genome, $blastResult ) = @_;
	my( $start ) = $blastResult->{alignment}{subject_start};
	my( $end ) = $blastResult->{alignment}{subject_end};
	my( $seq ) = $blastResult->{alignment}{subject};
	my( $pol ) = $blastResult->{alignment}{subject_pol};
	my( $seq_idx );
	my( $nt );
	my( $g_nt );
	my( $flag ) = 1;
	my( %REVERSE ) = ("A"=>"T","T"=>"A","C"=>"G","G"=>"C","a"=>"T","t"=>"A","c"=>"G","g"=>"C");
 
	if( $pol )
	{
		for( my $i=$start, $seq_idx=0; $i<=$end; $i++,$seq_idx++ )
		{
			while( (($nt=substr($seq,$seq_idx,1)) cmp '-')==0 ) { $seq_idx++; }
			$g_nt = substr($genome->[0],$i-1,1);
			if( exists($REVERSE{$nt}) && exists($REVERSE{$g_nt}) )
			{
				if( (uc($nt) cmp uc($g_nt)) != 0 )
				{
					$flag = 0;
					printf( STDERR "Match error: blastn_seq '%s'\tgenome_seq '%s'\tat position: %s\n", $nt, $g_nt, $i );
					last;
				}
			}
		}
	}
	else
	{
		for( my $i=$start, $seq_idx=0; $i>=$end; $i--,$seq_idx++ )
                {
                        while( (($nt=substr($seq,$seq_idx,1)) cmp '-')==0 ) { $seq_idx++; }
                        $g_nt = substr($genome->[0],$i-1,1);
			if( exists($REVERSE{$nt}) && exists($REVERSE{$g_nt}) )
			{
				if( ($REVERSE{$nt} cmp uc($g_nt)) != 0 )
				{
					$flag = 0;
					printf( STDERR "Match error: blastn_seq '%s'\treverse_blastn_seq '%s'\tgenome_seq '%s'\tat position: %s\n", $nt, $REVERSE{$nt}, $g_nt, $i );
					last;
				}
			}
			#else
			#{
			#	printf( STDERR "Sequence error for blastn_seq '%s' position %s: reverse complement not found\n", $nt, $i );
			#	exit;
			#}
		}
	}

	return $flag;
}

sub SEQ_FASTA_ReadFile
{
        my( $FN_Fasta, $FastaData, $getID ) = @_;
        my( $FH_Fasta );
        my( $flagFirst );
        my( $seq );
        my( $ID );
        my( $info );

        open( $FH_Fasta, $FN_Fasta ) or die( "Cannot open file: $FN_Fasta\n" );
        $seq = '';
        $flagFirst = 1;
        while( <$FH_Fasta> )
        {
                chomp; # remove the return character to make later operation easier.
                # '>' character is the starting character for the information line of each sequence in FASTA file.
                # After '>' character, there are several sequence lines.
                # The sequence ends at the next '>' character.
                if( $flagFirst && substr($_,0,1) =~ />/ )
                {
                        $info = $_;
                        $ID = &$getID($info);
                        $flagFirst = 0;
                }
                elsif( !$flagFirst && substr($_,0,1) =~ />/ )
                {
                        push( @{$FastaData->{$ID}}, $seq, $info );
                        $seq = '';
                        $info = $_;
                        $ID = &$getID($info);
                }
                else
                {
                        $seq .= $_;
                }
        }
        close( $FH_Fasta );
        push( @{$FastaData->{$ID}}, $seq, $info );
}

sub SEQ_BLAST_ParseBLASTResult_Total
{
        my( $flagBlast, $fileNameBlastResult, $blastResult ) = @_;
        my( $FH_blast );
        my( $flagQueryInfo ) = 0;
	my( $flagNewLine ) = 0;
        my( $counter ) = -1;
	my( $pol );
	my( $pol_a, $pol_b );
        my( @lineMem );
        my( @lineMem2 );

        open( $FH_blast, $fileNameBlastResult ) or die( "Cannot open file: $fileNameBlastResult\n" );
        while( <$FH_blast> )
        {
                if( (!$flagQueryInfo) && /^Query/ )
                {
                        chomp( @lineMem = split("=") );
                        $blastResult->{query}{info} = $lineMem[1];
                        $flagQueryInfo = 1;

                        while( <$FH_blast> )
                        {
                                if( /^Length/ )
                                {
                                        chomp( @lineMem = split( "=" ) );
                                        $blastResult->{query}{length} = $lineMem[1];
                                        last;
                                }
                        }
                }
                elsif( /^>/ )
                {
                        $counter++;
                        @lineMem = split(" ");
                        $blastResult->{result}[$counter]{id} = $lineMem[1];
                        $blastResult->{result}[$counter]{alignment}{query} = '';
                        $blastResult->{result}[$counter]{alignment}{subject} = '';
                        $blastResult->{result}[$counter]{alignment}{query_pol} = 1; # 1-positive, 0-negative
                        $blastResult->{result}[$counter]{alignment}{subject_pol} = 1;
                        $blastResult->{result}[$counter]{alignment}{query_start} = -1;
                        $blastResult->{result}[$counter]{alignment}{query_end} = -1;
                        $blastResult->{result}[$counter]{alignment}{subject_start} = -1;
                        $blastResult->{result}[$counter]{alignment}{subject_end} = -1;

                        while( <$FH_blast> )
                        {
                                if( /^Length/ )
                                {
                                        $flagNewLine = 0;
                                        chomp( @lineMem = split("=") );
                                        $blastResult->{result}[$counter]{seq_length} = $lineMem[1];
                                }
                                elsif( /Score/ )
                                {
                                        $flagNewLine = 0;
                                        $_ =~ s/,//g;
                                        chomp( @lineMem = split(" ") );
                                        $blastResult->{result}[$counter]{score} = $lineMem[2];
                                        $blastResult->{result}[$counter]{expect} = $lineMem[7];
                                }
                                elsif( /Identities/ )
                                {
                                        $flagNewLine = 0;
                                        chomp( @lineMem = split(" ") );
                                        @lineMem2 = split( '/', $lineMem[2] );
                                        $blastResult->{result}[$counter]{align_length} = $lineMem2[1];
                                        $blastResult->{result}[$counter]{identity} = $lineMem2[0];
                                        @lineMem2 = split( '/', $lineMem[6] );
                                        $blastResult->{result}[$counter]{gap} = $lineMem2[0];
                                        #@lineMem2 = split( '/', $lineMem[10] );
                                        #$blastResult->{result}[$counter]{gap} = $lineMem2[0];
                                }
				elsif( /Strand=(Plus|Minus)\/(Plus|Minus)/ )
				{
					$pol_a = $1;
					$pol_b = $2;
					if( $pol_a=~/Plus/ ) { $blastResult->{result}[$counter]{alignment}{query_pol}=1; }
					else { $blastResult->{result}[$counter]{alignment}{query_pol}=0; }
					if( $pol_b=~/Plus/ ) { $blastResult->{result}[$counter]{alignment}{subject_pol}=1; }
					else { $blastResult->{result}[$counter]{alignment}{subject_pol}=0; }
				}
                                elsif( /^Query/ )
                                {
                                        $flagNewLine = 0;
					#$pol = $blastResult->{result}[$counter]{alignment}{query_pol};

                                        chomp( @lineMem = split(" ") );
                                        $blastResult->{result}[$counter]{alignment}{query} .= $lineMem[2];
					if( $blastResult->{result}[$counter]{alignment}{query_start} < 0 )
					{
						$blastResult->{result}[$counter]{alignment}{query_start} = $lineMem[1];
					}
					$blastResult->{result}[$counter]{alignment}{query_end} = $lineMem[3];
						#if( $lineMem[1] < $blastResult->{result}[$counter]{alignment}{query_start} )
						#{
						#	$blastResult->{result}[$counter]{alignment}{query_start} = $lineMem[1];
						#}
						#if( $lineMem[3] > $blastResult->{result}[$counter]{alignment}{query_end} )
						#{
						#	$blastResult->{result}[$counter]{alignment}{query_end} = $lineMem[3];
						#}
                                }
                                elsif( /^Sbjct/ )
                                {
                                        $flagNewLine = 0;
                                        chomp( @lineMem = split(" ") );
                                        $blastResult->{result}[$counter]{alignment}{subject} .= $lineMem[2];

					if( $blastResult->{result}[$counter]{alignment}{subject_start} < 0 )
					{
						$blastResult->{result}[$counter]{alignment}{subject_start} = $lineMem[1];
					}
					$blastResult->{result}[$counter]{alignment}{subject_end} = $lineMem[3];
                                }
                                elsif( /^\n/ )
                                {
                                        $flagNewLine++;
                                }

                                if( $flagNewLine == 2 )
                                {
                                        $flagNewLine = 0;
                                        last;
                                }
                        }
                }
                elsif( /^\sScore/ )
                {
                        $counter++;
                        $_ =~ s/,//g;
                        @lineMem = split(" ");
                        $blastResult->{result}[$counter]{id} = $blastResult->{result}[$counter-1]{id};
                        $blastResult->{result}[$counter]{seq_length} = $blastResult->{result}[$counter-1]{seq_length};
			$blastResult->{result}[$counter]{scanfold} = $blastResult->{result}[$counter-1]{scanfold};
                        $blastResult->{result}[$counter]{score} = $lineMem[2];
                        $blastResult->{result}[$counter]{expect} = $lineMem[7];
                        $blastResult->{result}[$counter]{alignment}{query} = '';
                        $blastResult->{result}[$counter]{alignment}{subject} = '';
                        $blastResult->{result}[$counter]{alignment}{query_pol} = 1;
                        $blastResult->{result}[$counter]{alignment}{subject_pol} = 1;
                        $blastResult->{result}[$counter]{alignment}{query_start} = -1;
                        $blastResult->{result}[$counter]{alignment}{query_end} = -1;
                        $blastResult->{result}[$counter]{alignment}{subject_start} = -1;
                        $blastResult->{result}[$counter]{alignment}{subject_end} = -1;

                        while( <$FH_blast> )
                        {
                                if( /Identities/ )
                                {
                                        $flagNewLine = 0;
                                        chomp( @lineMem = split(" ") );
                                        @lineMem2 = split( '/', $lineMem[2] );
                                        $blastResult->{result}[$counter]{align_length} = $lineMem2[1];
                                        $blastResult->{result}[$counter]{identity} = $lineMem2[0];
                                        @lineMem2 = split( '/', $lineMem[6] );
                                        $blastResult->{result}[$counter]{gap} = $lineMem2[0];
                                        #@lineMem2 = split( '/', $lineMem[10] );
                                        #$blastResult->{result}[$counter]{gap} = $lineMem2[0];
                                }
                                elsif( /^Query/ )
                                {
                                        $flagNewLine = 0;
                                        chomp( @lineMem = split(" ") );
                                        $blastResult->{result}[$counter]{alignment}{query} .= $lineMem[2];
					if( $blastResult->{result}[$counter]{alignment}{query_start} < 0 )
					{
						$blastResult->{result}[$counter]{alignment}{query_start} = $lineMem[1];
					}
					$blastResult->{result}[$counter]{alignment}{query_end} = $lineMem[3];
                                }
                                elsif( /^Sbjct/ )
                                {
                                        $flagNewLine = 0;
                                        chomp( @lineMem = split(" ") );
                                        $blastResult->{result}[$counter]{alignment}{subject} .= $lineMem[2];
					if( $blastResult->{result}[$counter]{alignment}{subject_start} < 0 )
					{
						$blastResult->{result}[$counter]{alignment}{subject_start} = $lineMem[1];
					}
					$blastResult->{result}[$counter]{alignment}{subject_end} = $lineMem[3];
                                }
				elsif( /Strand=(Plus|Minus)\/(Plus|Minus)/ )
                                {
                                        $pol_a = $1;
                                        $pol_b = $2;
                                        if( $pol_a=~/Plus/ ) { $blastResult->{result}[$counter]{alignment}{query_pol}=1; }
                                        else { $blastResult->{result}[$counter]{alignment}{query_pol}=0; }
                                        if( $pol_b=~/Plus/ ) { $blastResult->{result}[$counter]{alignment}{subject_pol}=1; }
                                        else { $blastResult->{result}[$counter]{alignment}{subject_pol}=0; }
                                }
                                elsif( /^\n/ )
                                {
                                        $flagNewLine++;
                                }

                                if( $flagNewLine == 2 )
                                {
                                        $flagNewLine = 0;
                                        last;
                                }
                        }
                }
                elsif( m/No hits found/ )
                {
                        $counter = 0;
                        $blastResult->{result}[$counter]{id} = 'N/A';
                        $blastResult->{result}[$counter]{seq_length} = 'N/A';
                        $blastResult->{result}[$counter]{score} = 'N/A';
                        $blastResult->{result}[$counter]{expect} = 'N/A';
                        $blastResult->{result}[$counter]{alignment}{query} = 'N/A';
                        $blastResult->{result}[$counter]{alignment}{subject} = 'N/A';
                        $blastResult->{result}[$counter]{alignment}{query_start} = 'N/A';
                        $blastResult->{result}[$counter]{alignment}{query_end} = 'N/A';
                        $blastResult->{result}[$counter]{alignment}{subject_start} = 'N/A';
                        $blastResult->{result}[$counter]{alignment}{subject_end} = 'N/A';
                        $blastResult->{result}[$counter]{align_length} = 'N/A';
                        $blastResult->{result}[$counter]{identity} = 'N/A';
                        $blastResult->{result}[$counter]{positive} = 'N/A';
                        $blastResult->{result}[$counter]{gap} = 'N/A';
                }
        }
        close( $FH_blast );
}
