#! /usr/bin/env python

import os,sys

taxa_list = ["GEOFO","TAEGU"]
#taxa_list = ["CORBR"]

def read_fasta(fastafile):
    infile = open(fastafile,"r")
    seqDict = {} #list of sequence objects
    templab = ""
    tempseq = ""
    First = True
    for i in infile:
        if i[0] == ">":
            if First: First = False
            else:
                seqDict[templab] = tempseq # include an empty key and value.
            templab = i.strip()[1:]
            tempseq = ""
        else:
            tempseq = tempseq + i.strip()
    infile.close()
    seqDict[templab] = tempseq
    return seqDict
    
    
def rename(inputfas,outputfas):
    infile = open(inputfas,"rU")
    outfile = open(outputfas,"w")
    for i in infile:
        if i[0] == ">":
            split_list = i.strip().split(" ")
            GI = split_list[0].split("|")[1]
            species = split_list[1]
            outfile.write(">"+species[:4]+"@"+GI+"\n")
        else:
            outfile.write(i)
    infile.close()
    outfile.close()


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python "+sys.argv[0]+" genomeDIR intronDIR outDIR"
        sys.exit(0)
    genomeDIR,intronDIR,outDIR = sys.argv[1:]
    genomeDIR = os.path.abspath(genomeDIR)+"/"
    intronDIR = os.path.abspath(intronDIR)+"/"
    outDIR = os.path.abspath(outDIR)+"/"
    
    # change genome fasta file sequence names and make database
    """for gf in os.listdir(genomeDIR):
        if not gf.endswith("fa"): continue
        species = gf.split("_")[0]
        outname = genomeDIR+species+".fas"
        rename(genomeDIR+gf,outname)
        cmd = "makeblastdb -in "+outname+" -parse_seqids -dbtype nucl -out "+outname
        os.system(cmd)"""
    # extract intron from intronDIR and put outfile in outDIR
    #intron_out = open(outDIR+"introns.fas","w")
    outgroup = open(outDIR+"CORBR.fas","w")
    for folder in os.listdir(intronDIR):
        if folder == ".DS_Store": continue
    	print folder
        inputfasta = intronDIR+folder+"/sate.ind.intron.concatenated.noout.noallgap.fasta"
        seqDict = read_fasta(inputfasta)
        for i in seqDict.keys():
        	if i == "CORBR":
        		outgroup.write(">CORBR@"+folder+"\n"+seqDict[i]+"\n")
    outgroup.close()
        """count = 0
        Geos = ""
        Taen = ""
        for i in seqDict.keys():
            if i == taxa_list[0]:
			    count += 1
			    Geos = i
            elif i == taxa_list[1]:
                count += 1
                Taen = i
        if count == 2:
            intron_out.write(">Geos@"+folder+"\n"+seqDict[Geos]+"\n")
            intron_out.write(">Taen@"+folder+"\n"+seqDict[Taen]+"\n")"""
    intron_out.close()