Files used for analyses of passerine YME1L sequences:

Data files:
	1. passerineYME1L_nt.nex - used for PAUP analyses
	2. passerineYME1L_nt.phy - used for codeml
	3. passerineYME1L_aa.nex - translation of passerineYME1L_nt.nex

Output files:
	4. passerineYME1L_nt.tre - trees from PAUP analysis
		- two trees, optimal tree given HKY+I+G and bootstrap MRE consensus tree
	5. passerineYME1L_codeml_KaKs.txt
		- results of codeml analyses. These analyses reflect the comparison of a model
		  with 1 dN/dS (Ka/Ks) ratio model to a model with 2 Ka/Ks (dN/dS) ratios. These
		  analyses were conducted using two trees (the trees in passerineYME1L_nt.tre)
		 
As described in the narrative for the supporting materials, these files were used to test
the hypothesis that YME1L played a role in the burst of numt insertions. No compelling
evidence was uncovered from the dN/dS analysis.
