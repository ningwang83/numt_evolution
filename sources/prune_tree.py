#! /usr/bin/env python

""" use this script to prune trees to leave only taxa wanted. """

import os,sys
from node import Node
import tree_reader

"""taxa_list = ["Acanthisitta_chloris", "Amazona_aestiva", "Amazona_vittata", "Anas_platyrhynchos", \
			"Anser_cygnoides", "Caprimulgus_carolinensis", "Apaloderma_vittatum", \
			"Aptenodytes_forsteri", "Aquila_chrysaetos", "Ara_macao", "Balearica_regulorum", "Buceros_rhinoceros", \
			"Calidris_pugnax", "Calypte_anna", "Cariama_cristata", "Cathartes_aura", "Chaetura_pelagica", \
			"Charadrius_vociferus", "Chlamydotis_macqueenii", "Colinus_virginianus", "Colius_striatus", "Columba_livia", \
			"Corvus_brachyrhynchos", "Corvus_cornix", "Coturnix_japonica", "Cuculus_canorus", "Egretta_garzetta", \
			"Eurypyga_helias", "Falco_cherrug", "Falco_peregrinus", "Ficedula_albicollis", "Fulmarus_glacialis", \
			"Gallus_gallus", "Gavia_stellata", "Geospiza_fortis", "Haliaeetus_albicilla", "Haliaeetus_leucocephalus", \
			"Leptosomus_discolor", "Manacus_vitellinus", "Meleagris_gallopavo", "Melopsittacus_undulatus", "Merops_nubicus", \
			"Mesitornis_unicolor", "Nestor_notabilis", "Nipponia_nippon", "Opisthocomus_hoazin", "Pelecanus_crispus", \
			"Phaethon_lepturus", "Phalacrocorax_carbo", "Phoenicopterus_ruber", "Picoides_pubescens", "Podiceps_cristatus", \
			"Pseudopodoces_humilis", "Pterocles_gutturalis", "Pygoscelis_adeliae", "Serinus_canaria", "Struthio_camelus", \
			"Sturnus_vulgaris", "Taeniopygia_guttata", "Tauraco_erythrolophus", "Tinamus_guttatus", "Tyto_alba", \
			"Zonotrichia_albicollis", "Zosterops_lateralis"]"""

taxa_list = ["Acanthisitta_chloris", "Agapornis_roseicollis", "Amazona_aestiva", "Amazona_vittata", "Anas_platyrhynchos", \
			"Anas_zonorhyncha", "Anser_brachyrhynchus", "Anser_cygnoides", "Caprimulgus_carolinensis", "Apaloderma_vittatum", \
			"Aptenodytes_forsteri", "Apteryx_australis", "Aquila_chrysaetos", "Ara_macao", "Balearica_regulorum", "Buceros_rhinoceros", \
			"Calidris_pugnax", "Callipepla_squamata", "Calypte_anna", "Cariama_cristata", "Cathartes_aura", "Chaetura_pelagica", \
			"Charadrius_vociferus", "Chlamydotis_macqueenii", "Ciconia_boyciana", "Colinus_virginianus", "Colius_striatus", "Columba_livia", \
			"Corvus_brachyrhynchos", "Corvus_cornix", "Coturnix_japonica", "Cuculus_canorus", "Egretta_garzetta", "Eurypyga_helias", \
			"Falco_cherrug", "Falco_peregrinus", "Ficedula_albicollis", "Fulmarus_glacialis", "Gallirallus_okinawae", "Gallus_gallus", \
			"Gavia_stellata", "Geospiza_fortis", "Grus_japonensis", "Haliaeetus_albicilla", "Haliaeetus_leucocephalus", \
			"Lepidothrix_coronata", "Leptosomus_discolor", "Lonchura_striata", "Lyrurus_tetrix", "Manacus_vitellinus", "Meleagris_gallopavo", \
			"Melopsittacus_undulatus", "Merops_nubicus", "Mesitornis_unicolor", "Nannopterum_auritus", "Nannopterum_brasilianus", \
			"Nannopterum_harrisi", "Nestor_notabilis", "Nipponia_nippon", "Numida_meleagris", "Opisthocomus_hoazin", "Parus_major", \
			"Passer_domesticus", "Patagioenas_fasciata", "Pelecanus_crispus", "Phaethon_lepturus", "Phalacrocorax_carbo", \
			"Phoenicopterus_ruber", "Phylloscopus_plumbeitarsus", "Phylloscopus_trochiloides", "Phylloscopus_trochilus", "Picoides_pubescens", \
			"Podiceps_cristatus", "Pseudopodoces_humilis", "Pterocles_gutturalis", "Pygoscelis_adeliae", "Saxicola_maurus", \
			"Serinus_canaria", "Setophaga_coronata_coronata", "Sporophila_hypoxantha", "Strix_occidentalis", "Struthio_camelus", "Sturnus_vulgaris", \
			"Taeniopygia_guttata", "Tauraco_erythrolophus", "Tinamus_guttatus", "Tympanuchus_cupido", "Tyto_alba", "Uria_lomvia", \
			"Urile_pelagicus", "Zonotrichia_albicollis", "Zosterops_lateralis"]

def get_front_labels(node):
	"""given a node, return a list of front tip labels"""
	leaves = node.leaves()
	return [i.label for i in leaves]

def overlap(target_list,taxa_list):
	ol = 0
	for i in target_list:
		if i in taxa_list:
			ol += 1
	return ol

def diff(target_list,taxa_list):
	dif = 0
	for i in target_list:
		if not i in taxa_list:
			#print i
			dif += 1
	return dif
	
	
def reroot(oldroot, newroot):
    v = [] #path to the root
    n = newroot
    while 1:
        v.append(n)
        if not n.parent: break
        n = n.parent
    v.reverse()
    for i, cp in enumerate(v[:-1]):
        node = v[i+1]
        # node is current node; cp is current parent
        #print node.label, cp.label
        cp.remove_child(node)
        node.add_child(cp)
        cp.length = node.length
        cp.label = node.label
    return newroot

def remove_kink(node,curroot):
	"""
	smooth the kink created by prunning
	to prevent creating orphaned tips
	after prunning twice at the same node
	refer to pyphlawd prune_branches_from_raxmltre.py for a better function to remove_kink
	"""
	if node == curroot and len(curroot.children) == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = reroot(curroot,curroot.children[1])
		else: curroot = reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot

def remove_tips(root,taxa_list):
	for i in root.leaves():
		if not i.label in taxa_list:
			print i.label
			par = i.prune()
	return root


if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python prune_tree.py inDIR input_treefile outtree_name"
		print "make sure the input trees have tip names consistent with the taxa list"
		print "provide the full path of input treefile"
		sys.exit(0)

	inDIR,intree,outtree = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outfile = open(inDIR+outtree,"w")
	# read the input trees
	infile = open(intree,"rU")
	curroot = tree_reader.read_tree_string(infile.readline().strip())
	# check out if all taxa in the tree
	root_taxa = get_front_labels(curroot)
	differents = diff(taxa_list,root_taxa)
	assert differents != 0, "some taxa are not in the tree"
	# prune tree
	while True and curroot != None:
		print curroot.get_newick_repr()
		totaltaxa = get_front_labels(curroot)
		dif_leave = diff(totaltaxa,taxa_list)
		if dif_leave == 0:
			break
		else: 
			curroot = remove_tips(curroot,taxa_list)
	# remove kinks on the tree!
	while True:
		count = 0
		for i in curroot.iternodes():
			if not i.istip and len(i.children) == 1:
				count += 1
				node,curroot = remove_kink(i,curroot)
				break
		if count == 0:
			break
	newroot = curroot.get_newick_repr()+";"
	outfile.write(newroot+"\n")
	outfile.close()
	infile.close()
			
