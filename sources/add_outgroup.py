#! /usr/bin/env python

import os,sys

outgroup = "CORBR"

def read_fasta(fastafile):
    infile = open(fastafile,"r")
    seqDict = {} #list of sequence objects
    templab = ""
    tempseq = ""
    First = True
    for i in infile:
        if i[0] == ">":
            if First: First = False
            else:
                seqDict[templab] = tempseq # include an empty key and value.
            templab = i.strip()[1:]
            tempseq = ""
        else:
            tempseq = tempseq + i.strip()
    infile.close()
    seqDict[templab] = tempseq
    return seqDict

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python "+sys.argv[0]+" outgroupfas ingroupfasDIR outDIR"
        print "ingroupfasDIR is the previous chosen fasta of ingroups"
        sys.exit(0)
    outgroupfas,ingroupDIR,outDIR = sys.argv[1:]
    ingroupDIR = os.path.abspath(ingroupDIR)+"/"
    outDIR = os.path.abspath(outDIR)+"/"
    
    seqOut = read_fasta(outgroupfas)
    intron_list = []
    # collect intron loci for outgroups
    for i in seqOut.keys():
        intron_num = i.split("@")[1]
        intron_list.append(intron_num)
    # read ingroup fas file, choose those have outgroups write in file
    for f in os.listdir(ingroupDIR):
        if not f.endswith("fa"): continue
        f_num = f.split(".")[0]
        if f_num in intron_list:
            outfile = open(outDIR+f,"w")
            key = outgroup+"@"+f_num
            outfile.write(">"+key+"\n"+seqOut[key]+"\n")
            seqIn = read_fasta(ingroupDIR+f)
            for k in seqIn.keys():
                outfile.write(">"+k+"\n"+seqIn[k]+"\n")
        	#outfile.close()
        	
        	
    
