## The process of conducting Numt analyses  

### Authors: Bin Liang, Ning Wang, Nan Li, Rebecca Kimball, Edward Braun


### Numt scan in 64 bird genomes  

1. download 64 genomic contigs or scaffold from NCBI database (December 10th, 2015)
2. download conspecific or closely related (when conspecific genomes are not available) complete mitochondrial genomes from ncbi
3. blast mitochondrial (query) against the corresponding nuclear genome (database)
	* blastn -evalue 1e-09 ... (word-size 28), BLASTN v 2.3.0+ (Altschul, 1997) (-task blastn)
	* numt must has least one flanking region, with the total length of two flanking regions was greater than 100bp
	* `python PATH/blastMt2genome.py genomeDIR mtDIR`
4. The number of numt hits were calculated for each species (Figure 1, Table S2).

### Identification of homologous numt loci in five passerine birds  

1. Numt comparative analyses were focused in five passerine species
	* _Ficedula albicollis_
	* _Taeniopygia guttata_
	* _Serinus canaria_
	* _Geospiza fortis_
	* _Zonotrichia albicollis_

2. For the five focal species, made their mitogenomes all start at Phe tRNA and end at 12S or 16S (1k /2k bp extened after the starting point). 

3. Blastn these Mt genomes against conspecific nuclear genomes. For Geo and Zono, we used **_Thraupis episcopus_**  
	* Repeated numts in different species were then filtered out after examination (mamually). 
	* Identification of homologous numt loci among five species.
		- we extended each numt hit to include 1 kb flanking regions (both left and right) on the nuclear genome of each species. (named FNF sequences)
		- Moreover, because BlastN is local blast that tends to fragment contiguous sequences due to the existence of highly diverged intersections, and discontinuous numts could also insert into the same locus, so numt hits not distantly separated (i.e., within 1 kb, the minimum length of a flanking sequence) were grouped into a single locus.
		- make a database for each species of the FNF sequences, cat into a large database
		- all by all blastn this FNF sequences 
		- if one FNF has hits in other species, manually check them out see if they are real homologs
		- If one FNF has no hit in other species, it indicated that there is probable no similar numt in other species. However, it's also possible that our previous blastn search lost some short numts (due to the relative smaller evalue). under this condition, we also conducted the following blastn search
		- Additionally,we used FNF as query to conduct BLASTN against the other four species genomes, which can not only confirm the occurrence of numt insertion in the query species, but also increase the chance of identifying short and/or more diverged numts in other species.
		- Detect homologous numt loci by aligned among five species. 
		- Label present (1), absent (0), or ambiguous (?, no detectable synteny) of numt in each locus of each species. 		 
	* we identified 1031 numt loci for the five species. 

4. Extract numt information from blast \*.out 
	* `python extract_information.py inDIR outname file_end`
	* inDIR includes \*.out files from each species (e.g., Table/Ficedula\_...\_clear.out)
	* these .out files are the output from `neighborLoci20160114.pl` program
	* the output of `extract_information` is `extract_informaion.txt`, which include:
		`Index GI query_start query_end subDirect sub_start sub_end`
	* copy and paste the contents into excel, organize a `Mt_plot.csv` including the numt position for all five species.
	* **generate a numt position distribution plots using Mt_plot.r**

	* orgainze a Table with Mt sequence length and numt length in it. 
	* build a `loci_list.txt` with loci name in it, this contain 1031 loci
	* extract numt length for unique or shared groups from all five species
		- modify `length_extraction.py`, _uncomment_ `read loci_list...`, comment `read Mt_Geo_Zon.txt...` section
		- `python length_extraction.py blast_organized_table loci_list outfile`
		- output file is `Extracted_length.txt`
	* extract Mt start and ends for Geo and Zono unique or shared loci
		- organize a table `Mt_Geo_Zon.txt` form the above output
		- modify `length_extraction.py`, _comment_ `read loci_list...`, _uncomment_ `read Mt_Geo_Zon.txt...`
		- `python length_extraction.py blast_organized_table Mt_Geo_Zon.txt Mt_rangge.txt`
		- organize the results into table `Mt_uniVSshare_plot.csv`
		- **generate a numt position distribution plots using Mt_uniVSshare.r**  

	* Construct Numt length distribution histograms for G. fortis and Z. albicollis 
		- orgainze a table of numt length in `Numt_length_distribution.csv`
		- **Built histograms using `Numt_lengthdist_final.r`**. 

5. Reconstruction of five-species time tree
	* Take the synthetic whole-bird phylogeny (`labelled_supertree_New_synth.tre.cn`) from Brown et al., 2017
	* `python prune_tree.py inDIR input_tree outtree_name(pruned\_tree.tre)`
	* map numt number of each species on this pruned tree (see above, Figure 1A)
	* Generate a time-calibartion tree for the focal five species
		- Original Jarvis et al. (2014) data
		- Using **introns-unfiltered-alignments-noout** folder
		- 3792 intron loci were extracted from Jarvis et al. (2014) for both _T. guttata_ and _G. fortis_ using `extract_intron.py` (modified the file before use)
			1. this will make database for each genome contigs from _F. albicollis_, _S. canaria_, and _Z. albicollis_, including sequences named as `Species@GI`.
			2. This will also extract intron loci include both Geo and Taen
			3. output intron sequences into a \*.introns.fas file.
		
		- Use this \*.introns.fas file to conduct BLASTN against the three generated databases
			* `blastn -evalue 10 -max_target_seqs 10 ...`
			* filter blast hit and write intron sequences for each loci
				- `python filter_blast_to_intron_loci.py fasDIR outDIR blast_results`
				- fasDIR contain three genomes fasta (seq@GI) and intron.fas
				- this will filter blast hit (keep the top hit for same query and subject species)
					* name is `PATH/filter.blast`
				- **write the fasta sequence into file (have duplicated name, original fasta)**
				- build directory named `ID_rename2tree`
					- filter out the file with less than 5 species
					- add number to each sequence name in case there are duplicate sequences. 
		- MAFFT v7 (Katoh and Standley 2013) was used to align sequences with “–genafpair –maxiterate 1000” 
			* `python mafft_wrapper.py DIR num_cores seqtype(dna/aa)`
			* `python phyutility_wrapper.py DIR min_column_occupancy(0.4) dna`
		- ML tree was inferred for each locus using RAxML v8.1.22 (Stamatakis 2014) with GTR+GAMMA model
			* `python raxml_wrapper.py DIR number_cores dna/aa`
		- remove potential error clusters and loci less than 800bp
			* `python brlength_distribution.py DIR file_end leftcutoff rightcutoff inter/tip`
			* check the results, loci with internal branch length > 0.1 are excluded
			* 1025 intron loci passed this filter
				- folder name is `PATH/brOK/with_duptaxa`
			* delete duptaxa from **original fasta** above using `delete_duptaxa.py`
				- final fasta contain sequences longer than 800bp.
				- 745 loci passed the filter
			* In order to incorporate a robust calibration date from Jarvis et al. (2014), we further added intron data from Corvus brachyrhynchos (CORBR) as a distant outgroup
				- extract CORBR's intron sequences from Jarvis' data
					1. `python extract_intron.py genomeDIR intronDIR outDIR`
					2. this will generate **CORBR.fas** file
				- `python add_outgroup.py outgroupfas ingroupfasDIR outDIR`
					* this generate 729 loci that contain CORBR

6. Build species tree and assess gene tree species tree congruence
	* After aligning sequences by MAFFT for the 729 loci, alignments were cleaned by Phyutility v2.2.6 
		- `python mafft_wrapper.py DIR num_cores seqtype(dna/aa)`
		- `python phyutility_wrapper.py DIR min_column_occupancy(0.4) dna`
	* ML tree was inferred for each locus using RAxML v8.1.22 (Stamatakis 2014) with GTR+GAMMA model
		- `python raxml_wrapper.py DIR number_cores dna/aa`
	* species tree reconstruction
		- using ASTRAL III to reconstruct coalescent species tree
	* Gene tree-species tree congruence analyses
		- We rooted these gene trees with C. brachyrhynchos by using `reroot_pxrr.py` (modified)
		- compare the rooted gene trees with the extracted synthesis tree topology 
		- `phyparts -a 1 -d DIR_genetree -v -m Species_tree -o conflict`
		- 627 all congruent gene trees were kept for later use
	
7. find the top 100 clock like trees using SortaData (Smith et al., 2017) 
	* `python src/get_var_length.py tree_folder --flend .rr --outf var.txt --outg CORBR`
	* `python src/get_bp_genetrees.py genetreeDIR specietree --flend .tre --outf bp.txt`
	* `python src/combine_results.py var.txt bp.txt --outf combine.txt`
	* `python src/get_good_genes.py combine.txt --max 100 --order 3,1,2 --outf clock_100`
	* move all 100 genes' `aln-cln` files into a separate dir
	* `python PATH/rename_fas_taxa.py .` (rename sequence name for concatenate)
	* concatenated the top100 genes `concatenate_matrices.py` into a super matrix. 
	* build ML tree using A partitioned (by locus) method in RAxML with GTR+GAMMA model
		- check out the `Numt_concat.model` file, make sure it's formatting correctly
		- `raxml -s Numt_concat.phy -q Numt_concat.model -m GTRCAT -n Numt_concat -p 9160814 -# 10`

8. build time calibrated tree
	* treePL (Smith & O'Meara, 2012) is used to estimate divergence time based on an autocorrelated penalized likelihood model.  
	* A fixed date (20.526 Mya, Jarvis et al., 2014) was assigned to the most recent common ancestor (MRCA) of _C. brachyrhynchos_ and _Z. albicollis_. 
		- setting can be find in `configure_nuc_fix.txt`
	* The best smoothing value (i.e., 1000) was determined by the Random Subsample and Replicate Cross Validation (RSRCV). 

9. CI test
	* The numt status of the above 1031 loci (present = 1, absent = 0, or unknown = ?) is used as categorical data. 
	* 957 loci were also tested
	* We conducted the Consistency Index (CI) calculation in Paup* 4.0a158 (Swofford 2002). 

10. Reconstruction of ancestral numt number
	* Based on loci without unknown status (957 loci), we first used a maximum parsimony (MP) approach to trace numt insertions on different branches of the five-species time tree. 
	* Besides, ancestral numt number was also reconstructed on the species tree in Mesquite 3.03 (Maddison and Maddison 2015) using ML and proportional branch lengths (Schluter et al. 1997, Pagel 1999) under the two-rate model (Assy. Mk). 
	* ML analyses were carried out for both the 957 loci, as well as the complete 1031 loci matrix, respectively. 