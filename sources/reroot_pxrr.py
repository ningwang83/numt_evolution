"""reroot trees with pxrr program"""


import sys,os
import tree_reader


Outgroups = "CORBR"


if len(sys.argv) != 4:
	print "usage: python reroot_ortho_pxrr.py inDIR outDIR file_end"
	sys.exit()

inDIR,outDIR,file_end = sys.argv[1:]

inDIR = os.path.abspath(inDIR)+"/"
outDIR = os.path.abspath(outDIR)+"/"

for tree in os.listdir(inDIR):
	if not tree.endswith(file_end): continue
	infile = open(inDIR+tree,"r")
	oneline = infile.readline().strip()
	root = tree_reader.read_tree_string(oneline)
	infile.close()
	# change tiplabel to tip names
	for tip in root.leaves():
		tipname = tip.label.split("@")[0]
		tip.label = tipname
	newtree = root.get_newick_repr(True)+";"
	outtree = outDIR+tree+".rename"
	outfile = open(outtree,"w")
	outfile.write(newtree)
	outfile.close()
	
	# reroot the outtree
	cmd = "pxrr -t "+outtree+" -g "+Outgroups+" -o "+outDIR+tree+".rr"
	os.system(cmd)
