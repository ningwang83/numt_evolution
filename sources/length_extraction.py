#! /usr/bin/env python 

import os,sys

# use this script to extract numt length and mt length for unique or shared groups

def cal_len(l,Locus_Dict): # l is the output of a loci line
	llist = l.strip().split("_")
	if len(llist) == 2:
		locus = l.strip()
		if locus in Locus_Dict:
			Mt_len = Locus_Dict[locus][0]
			Numt_len = Locus_Dict[locus][1]
			output = locus+"\t"+Mt_len+"\t"+Numt_len
			return output
	elif len(llist) == 3:
		species,lo1,lo2 = llist[:]
		locus1 = species+"_"+lo1
		locus2 = species+"_"+lo2
		if locus1 in Locus_Dict and locus2 in Locus_Dict:
			Mt_total = int(Locus_Dict[locus1][0]) + int(Locus_Dict[locus2][0])
			Numt_total = int(Locus_Dict[locus1][1]) + int(Locus_Dict[locus2][1])
			output = l.strip()+"\t"+str(Mt_total)+"\t"+str(Numt_total)
			return output
	elif len(llist) == 4:
		species,lo1,lo2,lo3 = llist[:]
		locus1 = species+"_"+lo1
		locus2 = species+"_"+lo2
		locus3 = species+"_"+lo3
		if locus1 in Locus_Dict and locus2 in Locus_Dict and locus3 in Locus_Dict :
			Mt_total = int(Locus_Dict[locus1][0]) + int(Locus_Dict[locus2][0]) + int(Locus_Dict[locus3][0])
			Numt_total = int(Locus_Dict[locus1][1]) + int(Locus_Dict[locus2][1]) + int(Locus_Dict[locus3][1])
			output = l.strip()+"\t"+str(Mt_total)+"\t"+str(Numt_total)
			return output

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python length_extraction.py blast_organized_table loci_list outfile"
		sys.exit(0)
	or_table,loci,outname = sys.argv[1:]
	Path = os.getcwd() + "/"
	# read in blast_oranized table and write into Dict
	Locus_Dict = {}
	Mt_startend = {}
	with open(or_table,"rU") as infile:
		for li in infile:
			locus = li.strip().split("\t")[0]
			Mt_start = li.strip().split("\t")[2]
			Mt_end = li.strip().split("\t")[3]
			Mt_length = li.strip().split("\t")[4]
			Numt_length = li.strip().split("\t")[-1]
			Locus_Dict[locus] = [Mt_length,Numt_length]
			Mt_startend[locus] = [Mt_start,Mt_end]
	infile.close()
	
	# read loci_list file and write to outfile
	"""handle = open(loci,"rU")
	outfile = open(Path+outname,"w")
	for l in handle:
		if not "&" in l.strip():
			outstr = cal_len(l,Locus_Dict)
			if not outstr == None:
				outfile.write(outstr+"\n")
		else:
			splist = l.strip().split("&")
			for i in splist:
				outstr = cal_len(i,Locus_Dict)
				if not outstr == None:
					outfile.write(outstr+"\t")
			outfile.write("\n")"""
			
	# read Mt_Geo_Zon.txt and write to Mtstart and end in outfile
	handle = open(loci,"rU")
	outfile = open(Path+outname,"w")
	for l in handle:
		locus = l.strip()
		if not locus in Mt_startend: continue
		start = Mt_startend[locus][0]
		end = Mt_startend[locus][1]
		outfile.write(locus+"\t"+start+"\t"+end+"\n")
	handle.close()
	outfile.close()
