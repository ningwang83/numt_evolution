#! /usr/bin/env python

import os,sys

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: "+sys.argv[0]+" genomeDIR MtDIR"
		sys.exit()
	genomeDIR,MtDIR = sys.argv[1:]
	genomeDIR = os.path.abspath(genomeDIR)+"/"
	MtDIR = os.path.abspath(MtDIR)+"/"
		
	for mt in os.listdir(MtDIR):
		if not mt.endswith(".txt"): continue
		genus = mt.split("_")[0]
		species = mt.split("_")[1]
		mtgenus = mt.split("_")[2]
		mtspecies = mt.split("_")[3]
		genome = genus+"_"+species
		mtgenome = mtgenus+"_"+mtspecies
		if not os.path.exists(genomeDIR+genome+"*.nhr"):
			cmdmkdb = "makeblastdb -in "+genomeDIR+genome+"*.fa -dbtype nucl"
			os.system(cmdmkdb)
		cmdblastn1 = "blastn -db "+genomeDIR+genome+"*.fa -query "+MtDIR+mt+" -task blastn -max_target_seqs 5000 -out "+MtDIR+genome+"_"+mtgenome+"_e9_max5000.out -evalue 1e-09 -outfmt 7"
		cmdblastn2 = "blastn -db "+genomeDIR+genome+"*.fa -query "+MtDIR+mt+" -task blastn -num_descriptions 5000 -num_alignments 5000 -out "+MtDIR+genome+"_"+mtgenome+"_e9.out -evalue 1e-09"
		os.system(cmdblastn1)
		os.system(cmdblastn2)


