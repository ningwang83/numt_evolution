#! /usr/bin/env python 

import os,sys

# use this script to extract information from blast results.out

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python extract_information.py inDIR outname file_end"
		sys.exit(0)

	inDIR,outname,file_end = sys.argv[1:]
	inDIR = os.path.abspath(inDIR)+"/"
	outfile = open(inDIR+outname,"w")
	outfile.write("Index\tGI\tquery_start\tquery_end\tsubDirect\tsub_start\tsub_end\n")
	
	for f in os.listdir(inDIR):
		if not f.endswith(file_end): continue
		species = f.split("_")[0]
		outfile.write("\n"+species+"\n")
		with open(inDIR+f,"rU") as infile:
			for l in infile:
				if len(l) < 3: continue
				left = l.strip().split(": ")[0]
				right = l.strip().split(": ")[1]
				if left.split(" ")[-2] == "subject" and left.split(" ")[-1] == "id":
					index = left.split(" ")[1]
					GI = right.split("|")[1]
					outfile.write("\n"+index+"\t"+GI+"\t")
				elif left.split(" ")[-2] == "query" and left.split(" ")[-1] == "range":
					query_start = right.split("\t")[-2]
					query_end = right.split("\t")[-1]
					outfile.write(query_start+"\t"+query_end+"\t")
				elif left.split(" ")[-2] == "subject" and left.split(" ")[-1] == "direction":
					subDirection = right.strip()
					outfile.write(right+"\t")
				elif left.split(" ")[-2] == "subject" and left.split(" ")[-1] == "range":
						sub_start = right.split("\t")[-2]
						sub_end = right.split("\t")[-1]
						outfile.write(sub_start+"\t"+sub_end+"\t")						
				else: continue
		infile.close()
	outfile.close()
