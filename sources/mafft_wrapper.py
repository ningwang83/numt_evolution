#! /usr/bin/env python

""" define a set of functions that might be useful for future coding"""

import os,sys
import subprocess
from subprocess import call
from stat import *

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

def mafft(fasta,thread,seqtype):
	alignment = fasta+".mafft.aln"
	if os.path.exists(alignment) and os.stat(alignment).st_size>0:
		return alignment
	seqDict=read_fasta(fasta)
	Numseq=len(seqDict)
	maxlength = 0
	for seqID in seqDict:
		seq = seqDict[seqID]
		maxlength = max(maxlength,len(seq.replace("-","")))

	assert Numseq >= 4, "less than 4 sequences in "+fasta
	if Numseq >= 1000 or maxlength >= 10000:
		cmd = "mafft --auto --thread "+str(thread)
	else: cmd = "mafft --genafpair --maxiterate 1000 --thread "+str(thread)
	# genafpair suitable for sequences containing large unalignable regions

	if seqtype == "dna":
		cmd += " --nuc "+fasta
	elif seqtype == "aa":
		tempfile = open(fasta+".tempAA","a")
		for seqID in seqDict:
			seq = seqDict[seqID]
			seq = seq.upper().replace("U","X")
			if "*" in seq:
				seq = seq[:seq.find("*")]
			tempfile.write(">"+seqID+"\n"+seq+"\n")
		tempfile.close()
		cmd += " --amino "+fasta+".tempAA"		
	#else: print "Input data type is not aa or dna"
	outaln = open(alignment,"a")
	print cmd
	call(cmd,shell=True,stdout=outaln)
	outaln.close()
	if os.path.exists(fasta+".tempAA"): os.remove(fasta+".tempAA")

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python mafft_wrapper.py DIR num_cores seqtype(dna/aa)"
		print "seqence type needs to be dna or aa (lower case)"
		sys.exit(0)
		# minimum number of sequences should be bigger than the minimum taxa in the cluster files
	WORKDIR,num_cores,seqtype = sys.argv[1:]
	WORKDIR = os.path.abspath(WORKDIR) + "/"

	assert seqtype == "aa" or seqtype == "dna","Input data type: dna or aa"
	for fasta in os.listdir(WORKDIR):
		if not fasta.endswith(".fa"): continue
		seqDict = read_fasta(WORKDIR+fasta)
		assert len(seqDict) >= 4, "less than 4 species in the "+fasta
		maxlength = 0
		for seqID in seqDict:
			seq = seqDict[seqID]
			maxlength = max(maxlength,len(seq.replace("-","")))
		if (maxlength>=10000 and seqtype=="aa") or (maxlength>=30000 and seqtype=="dna"):
			print "%s contain sequences with %d as the longest." % (file, maxlength)
			print "Warning: sequence too long. May crash alignment process"
		mafft(WORKDIR+fasta,num_cores,seqtype)

