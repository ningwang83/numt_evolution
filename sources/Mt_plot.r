setwd("~/Dropbox/numt/final_documents/Table/")
par(mfrow=c(1,1))

# read in data
dat <- read.csv("Mt_plot.csv", header = TRUE);


# function
foo <- function(qstart,qend,cnumber,yspace,colo,times){
  for (i in 1:length(dat[,qstart])) {
    segments(x0=dat[i,qstart], y0=dat[i,cnumber]*times+yspace, x1=dat[i,qend], col=colo, lty=1, lwd=1);
  }
  
}

distt <- function(qstart,qend){
  freq <- numeric(19000)
  for (i in 1:length(dat[,qstart])) {
    if (is.na(dat[i,qstart])) {
      break;
    }
    ns = dat[i,qstart]
    ne = dat[i,qend]
    freq[ns:ne] <- freq[ns:ne] + 1;
  }
  return(freq)
} 

#xmax <- 17000;
#ymax <- 4000;

# Geo
# create empty plot
plot(NULL, t="n", lwd=3, xlab="Nucleotide Position", ylab="", yaxt='n', bty='l', cex.axis=1, cex.lab=1.25,
     xlim=c(1,19000), ylim=c(1,4500));
# Geo_plot
foo(2,3,1,350,"green",5)

options(max.print = 19000)
Geo_freq <- distt(2,3)
Geo_freq
barplot(Geo_freq*5, space = 0, col=rgb(153,51,255,255,maxColorValue = 255), add = TRUE)

# Zon
#plot(NULL, t="n", lwd=3, xlab="Nucleotide Position", ylab="", yaxt='n', main = "Zonotrichia", bty='l', cex.axis=1, cex.lab=1.25,
#     xlim=c(1,17000), ylim=c(1,4000));
foo(10,11,1,600,"purple",6)
Zon_freq <- distt(10,11)
Zon_freq
barplot(Zon_freq*5, space = 0, col=rgb(0,255,0,150,maxColorValue = 255), add = TRUE)


#plot(NULL, t="n", lwd=3, xlab="Nucleotide Position", ylab="", yaxt='n', bty='l', cex.axis=1, cex.lab=1.25,
#     xlim=c(1,17000), ylim=c(1,500));
# Fic
foo(4,5,1,80,"blue",35)
Fic_freq <- distt(4,5)
Fic_freq
# Ser
foo(6,7,1,150,"orange",30)
Ser_freq <- distt(6,7)
Ser_freq
# Tae
foo(8,9,1,0,"red",30)
Tae_freq <- distt(8,9)
Tae_freq

legend(20,4500, legend=c("Serinus", "Ficedula", "Taeniopygia", "Zonotrichia", "Geospiza"), text.font=3, col=c("orange", "blue", "red", "purple", "green"), lty=1, box.lty=0)

#legend("topleft", legend=c("Serinus", "Ficedula", "Taeniopygia", "Zonotrichia", "Geospiza"), text.font=3, col=c("orange", "blue", "red", "purple", "green"), lty=1, box.lty=0)

#basic command
#for (i in 1:length(dat[,1])) {
#  segments(x0=dat[i,2], y0=dat[i,1], x1=dat[i,3], col="black", lty=1);
#}

