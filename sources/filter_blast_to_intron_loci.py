#! /usr/bin/env python


"""
blast output file columns (separated by tab):
0-qseqid 1-qlen 2-sseqid 3-slen 4-frames 5-pident 6-nident 7-length
8-mismatch 9-gapopen 10-qstart 11-qend 12-sstart 13-send 14-evalue 15-bitscore

"""

import os,sys

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

def reverse_com(seq):
	reverse_seq = seq[::-1]
	compliment = ""
	for i in reverse_seq:
		if i == "A":
			compliment += "T"
		elif i == "T":
			compliment += "A"
		elif i == "C":
			compliment += "G"
		elif i == "G":
			compliment += "C"
		else:
			compliment += "N"
	return compliment
		

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" fasDIR outDIR blast_results"
		print "fasDIR contain three genomes fasta and intron.fas"
		print "outDIR contain intron loci each contain five species"
		sys.exit(0)
	fasDIR,outDIR,blast_results = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"
	
	# read genome fastas into different dictionary
	"""for fas in os.listdir(fasDIR):
		sp_name = fas.split(".")[0]
		if sp_name == "Ficedula":
			FicedulaDic = read_fasta(fasDIR+fas)
		elif sp_name == "Serinus":
			SerinusDic = read_fasta(fasDIR+fas)
		elif sp_name == "introns":
			GeoTaenIntron = read_fasta(fasDIR+fas)
		elif sp_name == "Zonotrichia":
			ZonotriDic = read_fasta(fasDIR+fas)"""
	
	# read blast results and write filter
	"""temp_out = open(outDIR+"filter.blast","w")
	infile = open(blast_results,"rU")
	seq_list = []
	for l in infile:
		line = l.strip().split("\t")
		qaryID,hitID = line[0],line[2]
		if qaryID in set(seq_list) and hitID in set(seq_list): continue
		temp_out.write(l)
		seq_list.append(qaryID)
		seq_list.append(hitID)
	infile.close()
	temp_out.close()"""
	
	# read filted blast and write intron loci
	"""with open(outDIR+"filter.blast","rU") as handle:
			for li in handle:
				line = li.strip().split("\t")
				qaryID,hitID = line[0],line[2]
				hitname = hitID.split("@")[0]
				locusName = qaryID.split("@")[1]
				output = open(outDIR+locusName+".fa","a")
				frames = line[4].split("/")[1]
				qstart,qend = int(line[10]),int(line[11])
				sstart,send = int(line[12]),int(line[13])
				qaryseq = GeoTaenIntron[qaryID][qstart-1:qend]
				if hitname == "Fice":
					if frames == "1":
						hitseq = FicedulaDic[hitID][sstart-1:send]
					else:
						hitseq = reverse_com(FicedulaDic[hitID][send-1:sstart])
				elif hitname == "Seri":
					if frames == "1":
						hitseq = SerinusDic[hitID][sstart-1:send]
					else:
						hitseq = reverse_com(SerinusDic[hitID][send-1:sstart])
				elif hitname == "Zono":
					if frames == "1":
						hitseq = ZonotriDic[hitID][sstart-1:send]
					else:
						hitseq = reverse_com(ZonotriDic[hitID][send-1:sstart])
				
				output.write(">"+qaryID+"\n"+qaryseq+"\n")
				output.write(">"+hitID+"\n"+hitseq+"\n")
				output.close()
	handle.close()"""
	
	# filter loci contain five species
	if not os.path.exists(outDIR+"five"):
		os.mkdir(outDIR+"five")
	for f in os.listdir(outDIR):
		if not f.endswith("fa"): continue
		sp_list = []
		with open(outDIR+f,"rU") as infile:
			seqDict = {} #list of sequence objects
			templab = ""
			tempseq = ""
			label_list = []
			count = 0
			First = True
			for i in infile:
				if i[0] == ">":
					if First: First = False
					else:
						seqDict[templab] = tempseq # include an empty key and value.
					templab = i.strip()[1:]+"_"+str(count) # to due with duplicate label
					count += 1
					tempsp = i.strip().split("@")[0]
					sp_list.append(tempsp)
					tempseq = ""
				else:
					tempseq = tempseq + i.strip()
		infile.close()
		seqDict[templab] = tempseq
		if len(set(sp_list)) == 5:
			outfile = open(outDIR+"five/"+f,"w")
			for key in seqDict:
				outfile.write(">"+key+"\n"+seqDict[key]+"\n")
			#os.rename(outDIR+f, outDIR+"five/"+f)
			
					
			
				
				
