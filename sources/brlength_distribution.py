#! usr/bin/env python

import newick3,phylo3,os,sys,math
from tree_utils import *

if len(sys.argv) != 6:
    print "usage: python brlength_distribution.py DIR file_end leftcutoff rightcutoff inter/tip"
    sys.exit()

WORKDIR,file_end,leftcutoff,rightcutoff,argu = sys.argv[1:]
WORKDIR = os.path.abspath(WORKDIR)+"/"

internode_list = []

for i in os.listdir(WORKDIR):
    if not i.endswith(file_end): continue
    with open(WORKDIR+i,"r") as infile:
        intree = newick3.parse(infile.readline())
    for node in intree.iternodes():
        if argu == "inter":
            if node.istip or node == intree: continue
        elif argu == "tip":
            if not node.istip: continue
        if node.length >= float(leftcutoff) and node.length <= float(rightcutoff):
            print i
            internode_list.append(node.length)
"""
Brnum = len(internode_list)
print Brnum

string = " ".join(str(i) for i in internode_list)

print string

with open(WORKDIR+"brlength.distribute","a") as outfile:
        outfile.write(string)"""




