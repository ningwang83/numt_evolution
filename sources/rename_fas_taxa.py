#! /usr/bin/env python

import os,sys


def read_fasta(fastafile):
    infile = open(fastafile,"r")
    seqDict = {} #list of sequence objects
    templab = ""
    tempseq = ""
    First = True
    for i in infile:
        if i[0] == ">":
            if First: First = False
            else:
                seqDict[templab] = tempseq # include an empty key and value.
            templab = i.strip()[1:]
            tempseq = ""
        else:
            tempseq = tempseq + i.strip()
    infile.close()
    seqDict[templab] = tempseq
    return seqDict

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "python "+sys.argv[0]+" WORKDIR"
        sys.exit(0)
    WORKDIR = sys.argv[1]
    WORKDIR = os.path.abspath(WORKDIR)+"/"
    
    for f in os.listdir(WORKDIR):
        #print f
        if not f.endswith("aln-cln"): continue
        loci = f.split(".")[0]
        #print loci
        seqDict = read_fasta(WORKDIR+f)
        outfile = open(WORKDIR+loci+".rn.fa","w")
        for k in seqDict.keys():
            species = k.split("@")[0]
            outfile.write(">"+species+"\n"+seqDict[k]+"\n")
        outfile.close()
