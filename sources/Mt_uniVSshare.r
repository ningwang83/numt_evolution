setwd("~/Dropbox/numt/final_documents/Table/")
par(mfrow=c(1,1))

# read in data
dat <- read.csv("Mt_uniVSshare_plot.csv", header = TRUE);


# function
foo <- function(qstart,qend,cnumber,yspace,colo,times){
  for (i in 1:length(dat[,qstart])) {
    segments(x0=dat[i,qstart], y0=dat[i,cnumber]*times+yspace, x1=dat[i,qend], col=colo, lty=1, lwd=1);
  }
  
}

#Zon
plot(NULL, t="n", lwd=3, xlab="Mitochondrial Position", ylab="", yaxt='n', bty='l', main = "Zonotrichia", font.main=3, cex.axis=1, cex.lab=1.25,
     xlim=c(1,19000), ylim=c(1,1800));
# Geo_plot
foo(2,3,1,50,"green",5)
foo(4,5,1,60,"purple",5)
foo(6,7,1,20,"red",35)
foo(8,9,1,10,"blue",35)
foo(10,11,1,0,"orange",35)

legend(2500,1500, legend=c("Unique", "Share2", "Share3", "Share4", "Share5"), text.font=1, col=c("green", "purple", "red", "blue", "orange"), lty=1, lwd=2, box.lty=0)

#Geo
plot(NULL, t="n", lwd=3, xlab="Mitochondrial Position", ylab="", yaxt='n', bty='l', main = "Geospiza", font.main=3, cex.axis=1, cex.lab=1.25,
     xlim=c(1,19000), ylim=c(1,1600));
# Geo_plot
foo(12,13,1,60,"green",5)
foo(14,15,1,30,"purple",5)
foo(16,17,1,20,"red",35)
foo(18,19,1,10,"blue",35)
foo(20,21,1,0,"orange",35)

legend(2500,1500, legend=c("Unique", "Share2", "Share3", "Share4", "Share5"), text.font=1, col=c("green", "purple", "red", "blue", "orange"), lty=1, lwd=2, box.lty=0)
