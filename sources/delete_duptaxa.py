#! /usr/bin/env python


"""
blast output file columns (separated by tab):
0-qseqid 1-qlen 2-sseqid 3-slen 4-frames 5-pident 6-nident 7-length
8-mismatch 9-gapopen 10-qstart 11-qend 12-sstart 13-send 14-evalue 15-bitscore

"""

import os,sys

def read_fasta(fastafile):
	infile = open(fastafile,"r")
	seqDict = {} #list of sequence objects
	templab = ""
	tempseq = ""
	First = True
	for i in infile:
		if i[0] == ">":
			if First: First = False
			else:
				seqDict[templab] = tempseq # include an empty key and value.
			templab = i.strip()[1:]
			tempseq = ""
		else:
			tempseq = tempseq + i.strip()
	infile.close()
	seqDict[templab] = tempseq
	return seqDict

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python "+sys.argv[0]+" fasDIR outDIR cutofflen"
		print "make a new directory first"
		print "cutofflen is the minimum length of include squences in one loci"
		sys.exit(0)		
	fasDIR,outDIR,cutofflen = sys.argv[1:]
	fasDIR = os.path.abspath(fasDIR)+"/"
	outDIR = os.path.abspath(outDIR)+"/"

	for fas in os.listdir(fasDIR):
		if not fas.endswith("fa"): continue
		seqDict = read_fasta(fasDIR+fas)
		count = 0
		for seq in seqDict.values():
			if len(seq) < int(cutofflen):
				count += 1
		if count == 0:
			outfile = open(outDIR+fas,"w")
			for key in seqDict:
				outfile.write(">"+key+"\n"+seqDict[key]+"\n")
			outfile.close()

