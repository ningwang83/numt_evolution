setwd("~/Dropbox/numt/final_documents/Table")
a <- read.csv("Numt_length_distribution.csv", header = TRUE, stringsAsFactors=FALSE)
xmax <- 2000

par(mfrow=c(1,2))
#install.packages("plotrix")
require(plotrix)

# Geo
l <- list(a[,1],a[,8],a[,6])
multhist(l,col=c("red","purple","blue"), xlab="Sequence Length", ylab="Number", main="Geospiza", font.main=3, cex.axis=1, cex.lab=1.25, ylim=c(0,100))
legend("topright", c("Unique", "Share 2", "Share 2+"), text.font=2, col=c("red", "purple", "blue"), lty=1, lwd=2, box.lty=0)


# Zon
l <- list(a[,2],a[,7],a[,5])
multhist(l,col=c("red","purple","blue"), xlab="Sequence Length", ylab="Number", main="Zonotrichia", font.main=3, cex.axis=1, cex.lab=1.25, ylim=c(0,120))
legend("topright", c("Unique", "Share 2", "Share 2+"), text.font=2, col=c("red", "purple", "blue"), lty=1, lwd=2, box.lty=0)

